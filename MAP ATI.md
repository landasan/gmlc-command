MAP ATI (extraRequestedInfo=false to excluded imei, ms-classmark and mnpRequestedInfo params)
-----
curl -d "msisdn=601140827446&extraRequestedInfo=false" -X POST 10.1.2.20:8181/restcomm/gmlc/rest

curl -d "msisdn=60163334133&extraRequestedInfo=false&locationInfoEps=false" -X POST 10.1.2.20:8181/restcomm/gmlc/rest


**CELCOM - Network**

-----
**[ 3G ] IDLE - CGI**

---

{
  "network": "GSM",
  "protocol": "MAP",
  "operation": "ATI",
  "result": "SUCCESS",
  "CSLocationInformation": {
    "LocationNumber": {
      "oddFlag": true,
      "natureOfAddressIndicator": 4,
      "internalNetworkNumberIndicator": 1,
      "numberingPlanIndicator": 1,
      "addressPresentationRestrictedIndicator": 1,
      "screeningIndicator": 3,
      "address": 60199030053
    },
    "**CGI**": {
      "mcc": 502,
      "mnc": 19,
      "lac": 14702,
      "ci": 14165
    },
    "GeographicalInformation": {},
    "GeodeticInformation": {},
    "EPSLocationInformation": {
      "TAI": {},
      "ECGI": {},
      "GeographicalInformation": {},
      "GeodeticInformation": {}
    }
  },
  "PSLocationInformation": {
    "LSA": {},
    "RAI": {},
    "GeographicalInformation": {},
    "GeodeticInformation": {}
  },
  "GPRSMSClass": {},
  "MNPInfoResult": {},
  "saiPresent": false,
  "ageOfLocationInformation": 0,
  "currentLocationRetrieved": true,
  "msisdn": 601140827446,
  "vlrNumber": 60199030053,
  "mscNumber": 60199030053,
  "subscriberState": "assumedIdle"
}


**[ 4G ] IDLE - SAI**

---

{
  "network": "GSM",
  "protocol": "MAP",
  "operation": "ATI",
  "result": "SUCCESS",
  "CSLocationInformation": {
    "LocationNumber": {
      "oddFlag": true,
      "natureOfAddressIndicator": 4,
      "internalNetworkNumberIndicator": 1,
      "numberingPlanIndicator": 1,
      "addressPresentationRestrictedIndicator": 1,
      "screeningIndicator": 3,
      "address": 60199030053
    },
    "**SAI**": {
      "mcc": 502,
      "mnc": 19,
      "lac": 1472,
      "sac": 15079
    },
    "GeographicalInformation": {},
    "GeodeticInformation": {},
    "EPSLocationInformation": {
      "TAI": {},
      "ECGI": {},
      "GeographicalInformation": {},
      "GeodeticInformation": {}
    }
  },
  "PSLocationInformation": {
    "LSA": {},
    "RAI": {},
    "GeographicalInformation": {},
    "GeodeticInformation": {}
  },
  "GPRSMSClass": {},
  "MNPInfoResult": {},
  "saiPresent": true,
  "ageOfLocationInformation": 0,
  "currentLocationRetrieved": true,
  "msisdn": 60193303030,
  "vlrNumber": 60199030053,
  "mscNumber": 60199030053,
  "subscriberState": "assumedIdle"
}

---

** BUSY **

{
  "network": "GSM",
  "protocol": "MAP",
  "operation": "ATI",
  "result": "SUCCESS",
  "CSLocationInformation": {
    "LocationNumber": {
      "oddFlag": true,
      "natureOfAddressIndicator": 4,
      "internalNetworkNumberIndicator": 1,
      "numberingPlanIndicator": 1,
      "addressPresentationRestrictedIndicator": 1,
      "screeningIndicator": 3,
      "address": 60193900083
    },
    "SAI": {
      "mcc": 502,
      "mnc": 19,
      "lac": 1472,
      "sac": 12930
    },
    "GeographicalInformation": {},
    "GeodeticInformation": {},
    "EPSLocationInformation": {
      "TAI": {},
      "ECGI": {},
      "GeographicalInformation": {},
      "GeodeticInformation": {}
    }
  },
  "PSLocationInformation": {
    "LSA": {},
    "RAI": {},
    "GeographicalInformation": {},
    "GeodeticInformation": {}
  },
  "GPRSMSClass": {},
  "MNPInfoResult": {},
  "saiPresent": true,
  "ageOfLocationInformation": 0,
  "currentLocationRetrieved": true,
  "msisdn": 601140827446,
  "vlrNumber": 60193900083,
  "mscNumber": 60193900083,
  "subscriberState": "camelBusy"
}
---

** OFF (Idle) - before 4 hours**

{
  "network": "UMTS",
  "protocol": "MAP",
  "operation": "ATI",
  "subscriberIdentity": 601140827446,
  "result": "ERROR",
  "errorReason": "[MAP Dialog Timeout]"
}

{
  "network": "GSM",
  "protocol": "MAP",
  "operation": "ATI",
  "result": "SUCCESS",
  "CSLocationInformation": {
    "LocationNumber": {
      "oddFlag": true,
      "natureOfAddressIndicator": 4,
      "internalNetworkNumberIndicator": 1,
      "numberingPlanIndicator": 1,
      "addressPresentationRestrictedIndicator": 1,
      "screeningIndicator": 3,
      "address": 60193900083
    },
    "CGI": {
      "mcc": 502,
      "mnc": 19,
      "lac": 14702,
      "ci": 14165
    },
    "GeographicalInformation": {},
    "GeodeticInformation": {},
    "EPSLocationInformation": {
      "TAI": {},
      "ECGI": {},
      "GeographicalInformation": {},
      "GeodeticInformation": {}
    }
  },
  "PSLocationInformation": {
    "LSA": {},
    "RAI": {},
    "GeographicalInformation": {},
    "GeodeticInformation": {}
  },
  "GPRSMSClass": {},
  "MNPInfoResult": {},
  "saiPresent": false,
  "ageOfLocationInformation": 4,
  "currentLocationRetrieved": false,
  "msisdn": 601140827446,
  "vlrNumber": 60193900083,
  "mscNumber": 60193900083,
  "subscriberState": "assumedIdle"
}

---

** OFF (Imsidetach) - after 4 hours**

{
  "network": "GSM",
  "protocol": "MAP",
  "operation": "ATI",
  "result": "SUCCESS",
  "CSLocationInformation": {
    "LocationNumber": {
      "oddFlag": true,
      "natureOfAddressIndicator": 4,
      "internalNetworkNumberIndicator": 1,
      "numberingPlanIndicator": 1,
      "addressPresentationRestrictedIndicator": 1,
      "screeningIndicator": 3,
      "address": 60193900083
    },
    "SAI": {
      "mcc": 502,
      "mnc": 19,
      "lac": 1472,
      "sac": 12930
    },
    "GeographicalInformation": {},
    "GeodeticInformation": {},
    "EPSLocationInformation": {
      "TAI": {},
      "ECGI": {},
      "GeographicalInformation": {},
      "GeodeticInformation": {}
    }
  },
  "PSLocationInformation": {
    "LSA": {},
    "RAI": {},
    "GeographicalInformation": {},
    "GeodeticInformation": {}
  },
  "GPRSMSClass": {},
  "MNPInfoResult": {},
  "saiPresent": true,
  "ageOfLocationInformation": 300,
  "currentLocationRetrieved": false,
  "msisdn": 601140827446,
  "vlrNumber": 60193900083,
  "mscNumber": 60193900083,
  "subscriberState": "netDetNotReachable",
  "notReachableReason": "imsiDetached"
}

---

** OFF - after 24 hours**

{
  "network": "GSM",
  "protocol": "MAP",
  "operation": "ATI",
  "result": "SUCCESS",
  "CSLocationInformation": {
    "LocationNumber": {},
    "GeographicalInformation": {},
    "GeodeticInformation": {},
    "EPSLocationInformation": {
      "TAI": {},
      "ECGI": {},
      "GeographicalInformation": {},
      "GeodeticInformation": {}
    }
  },
  "PSLocationInformation": {
    "LSA": {},
    "RAI": {},
    "GeographicalInformation": {},
    "GeodeticInformation": {}
  },
  "GPRSMSClass": {},
  "MNPInfoResult": {},
  "saiPresent": false,
  "currentLocationRetrieved": false,
  "msisdn": 601140827446,
  "mscNumber": 60193900083,
  "subscriberState": "netDetNotReachable",
  "notReachableReason": "msPurged"
}