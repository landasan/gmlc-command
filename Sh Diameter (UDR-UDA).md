# Sh (UDR/UDA) PS domain (default)
curl -d "msisdn=60193303030&operation=UDR&token=L4ndas4n" -X POST 10.1.2.20:8282/restcomm/gmlc/rest

curl -d "msisdn=60193303030&operation=UDR&token=L4ndas4n&domain=ps" -X POST 10.1.2.20:8282/restcomm/gmlc/rest


## Result Respond
{
  "network": "IMS",
  "protocol": "Diameter Sh",
  "operation": "UDR-UDA",
  "result": "SUCCESS",
  "PublicIdentifiers": {
    "msisdn": 60193303030
  },
  "CSLocationInformation": {},
  "PSLocationInformation": {},
  "EPSLocationInformation": {
    "ECGI": {
      "mcc": 502,
      "mnc": 19,
      "eci": 38676245,
      "eNBId": 151079,
      "ci": 21
    },
    "TAI": {
      "mcc": 502,
      "mnc": 19,
      "tac": 774
    },
    "mmeName": "MMEC02.MMEGI8001.MME.EPC.MNC019.MCC502.3GPPNETWORK.ORG",
    "currentLocationRetrieved": true,
    "ageOfLocationInformation": 0
  },
  "5GSLocationInformation": {}
}

# Sh (UDR/UDA) CS domain
curl -d "msisdn=60193303030&operation=UDR&token=L4ndas4n&domain=cs" -X POST 10.1.2.20:8282/restcomm/gmlc/rest

## Result Respond
{
  "network": "IMS",
  "protocol": "Diameter Sh",
  "operation": "UDR-UDA",
  "result": "SUCCESS",
  "PublicIdentifiers": {
    "msisdn": 60193303030
  },
  "CSLocationInformation": {
    "LocationNumber": {
      "oddFlag": true,
      "natureOfAddressIndicator": 4,
      "internalNetworkNumberIndicator": 1,
      "numberingPlanIndicator": 1,
      "addressPresentationRestrictedIndicator": 1,
      "screeningIndicator": 3,
      "address": 60199030053
    },
    "SAI": {
      "mcc": 502,
      "mnc": 19,
      "lac": 1472,
      "sac": 15079
    },
    "currentLocationRetrieved": true,
    "ageOfLocationInformation": 0,
    "EPSLocationInformation": {}
  },
  "PSLocationInformation": {},
  "EPSLocationInformation": {},
  "5GSLocationInformation": {}
}

# ---DiGi
{
  "network": "IMS",
  "protocol": "Diameter Sh",
  "operation": "UDR",
  "msisdn": 601131272500,
  "result": "ERROR",
  "errorReason": "[DIAMETER_ERROR_USER_UNKNOWN, Diameter error code: 5001]"
}

# ---Umobile
{
  "network": "IMS",
  "protocol": "Diameter Sh",
  "operation": "UDR",
  "msisdn": 601162127384,
  "result": "ERROR",
  "errorReason": "[DIAMETER_ERROR_USER_UNKNOWN, Diameter error code: 5001]"
}

# -- Celcom 2G
{
  "network": "IMS",
  "protocol": "Diameter Sh",
  "operation": "UDR",
  "msisdn": 601140827446,
  "result": "ERROR",
  "errorReason": "[DIAMETER_ERROR_USER_DATA_NOT_AVAILABLE, Diameter error code: 4100]"
}
-------------------------------------------------------------------------------------------------------------------------

PS domain:

EPS location supported = TRUE; active location retrieval = TRUE:

curl -d "msisdn=60192235906&coreNetwork=ims" -X POST 192.168.1.38:8080/restcomm/gmlc/rest
EPS location supported = FALSE; active location retrieval = TRUE

curl -d "msisdn=60192235906&coreNetwork=ims&locationInfoEps=false" -X POST 192.168.1.38:8080/restcomm/gmlc/rest
EPS location supported = TRUE; active location retrieval = FALSE

curl -d "msisdn=60192235906&coreNetwork=ims&activeLocation=false" -X POST 192.168.1.38:8080/restcomm/gmlc/rest
EPS location supported = FALSE; active location retrieval = FALSE

curl -d "msisdn=60192235906&coreNetwork=ims&locationInfoEps=false&activeLocation=false" -X POST 192.168.1.38:8080/restcomm/gmlc/rest
CS domain:

EPS location supported = TRUE; active location retrieval = TRUE

curl -d "msisdn=60192235906&coreNetwork=ims&domain=cs" -X POST 192.168.1.38:8080/restcomm/gmlc/rest
EPS location supported = FALSE; active location retrieval = true

curl -d "msisdn=60192235906&coreNetwork=ims&domain=cs&locationInfoEps=false" -X POST 192.168.1.38:8080/restcomm/gmlc/rest
EPS location supported = TRUE; active location retrieval = FALSE

curl -d "msisdn=60192235906&coreNetwork=ims&domain=cs&activeLocation=false" -X POST 192.168.1.38:8080/restcomm/gmlc/rest
EPS location supported = FALSE; active location retrieval = FALSE

curl -d "msisdn=60192235906&coreNetwork=ims&domain=cs&locationInfoEps=false&activeLocation=false" -X POST 192.168.1.38:8080/restcomm/gmlc/rest
Then, if not set, default cURL parameter values for Sh would be:
domain = PS
locationInfoEps = true
activeLocation = true
