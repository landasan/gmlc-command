# SLh-SLg (RIR/RIA - PLR/PLA)

curl -d "coreNetwork=lte&msisdn=60193303030&clientReferenceNumber=1&slgLocationType=1&lcsNameString=kepong&lcsFormatIndicator=0&slgClientType=0&lcsCallbackUrl=http://localhost:8081/api/report&psiService=false" -X POST 10.1.1.20:8181/restcomm/gmlc/rest

## Diameter error respond
{
  "network": "LTE",
  "protocol": "Diameter SLh",
  "operation": "RIR",
  "subscriberIdentity": 60193303030,
  "clientReferenceNumber": 1,
  "result": "ERROR",
  "errorReason": "[DIAMETER_APPLICATION_UNSUPPORTED, Diameter error code: 3007]"
}