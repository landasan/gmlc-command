** SRISM-PSI (Default)(ECGI and TAI are retrieved from LTE if available) **
------------
curl -d "msisdn=601140827446&psiService=true" -X POST 10.1.1.20:8181/restcomm/gmlc/rest

** SRISM-PSI (locationInfoEps = false)(only CS domain location information will be retrieved) **
------------
curl -d "msisdn=601140827446&psiService=true&locationInfoEps=false" -X POST 10.1.1.20:8181/restcomm/gmlc/rest

---

** IDLE - CGI (2G)**

---

{
  "network": "GSM",
  "protocol": "MAP",
  "operation": "PSI",
  "result": "SUCCESS",
  "CSLocationInformation": {
    "LocationNumber": {
      "oddFlag": true,
      "natureOfAddressIndicator": 4,
      "internalNetworkNumberIndicator": 1,
      "numberingPlanIndicator": 1,
      "addressPresentationRestrictedIndicator": 1,
      "screeningIndicator": 3,
      "address": 60199030053
    },
    "CGI": {
      "mcc": 502,
      "mnc": 19,
      "lac": 14702,
      "ci": 14164
    },
    "GeographicalInformation": {},
    "GeodeticInformation": {},
    "EPSLocationInformation": {
      "TAI": {},
      "ECGI": {},
      "GeographicalInformation": {},
      "GeodeticInformation": {}
    }
  },
  "PSLocationInformation": {
    "LSA": {},
    "RAI": {},
    "GeographicalInformation": {},
    "GeodeticInformation": {}
  },
  "GPRSMSClass": {},
  "MNPInfoResult": {},
  "saiPresent": false,
  "ageOfLocationInformation": 0,
  "currentLocationRetrieved": true,
  "msisdn": 601140827446,
  "imsi": 502194100959766,
  "vlrNumber": 60199030053,
  "subscriberState": "assumedIdle"
}
---

** IDLE - SAI (3G)**


{
  "network": "GSM",
  "protocol": "MAP",
  "operation": "PSI",
  "result": "SUCCESS",
  "CSLocationInformation": {
    "LocationNumber": {
      "oddFlag": true,
      "natureOfAddressIndicator": 4,
      "internalNetworkNumberIndicator": 0,
      "numberingPlanIndicator": 1,
      "addressPresentationRestrictedIndicator": 3,
      "screeningIndicator": 3,
      "address": 60181000011
    },
    "SAI": {
      "mcc": 502,
      "mnc": 18,
      "lac": 12001,
      "sac": 11515
    },
    "GeographicalInformation": {},
    "GeodeticInformation": {},
    "EPSLocationInformation": {
      "TAI": {},
      "ECGI": {},
      "GeographicalInformation": {},
      "GeodeticInformation": {}
    }
  },
  "PSLocationInformation": {
    "LSA": {},
    "RAI": {},
    "GeographicalInformation": {},
    "GeodeticInformation": {}
  },
  "GPRSMSClass": {},
  "MNPInfoResult": {},
  "saiPresent": true,
  "ageOfLocationInformation": 0,
  "currentLocationRetrieved": true,
  "msisdn": 60189758742,
  "imsi": 502181130875271,
  "vlrNumber": 60181000011,
  "mscNumber": 60181000011,
  "subscriberState": "assumedIdle"
}
---

** Busy **

{
  "network": "GSM",
  "protocol": "MAP",
  "operation": "PSI",
  "result": "SUCCESS",
  "CSLocationInformation": {
    "LocationNumber": {
      "oddFlag": true,
      "natureOfAddressIndicator": 4,
      "internalNetworkNumberIndicator": 1,
      "numberingPlanIndicator": 1,
      "addressPresentationRestrictedIndicator": 1,
      "screeningIndicator": 3,
      "address": 60193900083
    },
    "CGI": {
      "mcc": 502,
      "mnc": 19,
      "lac": 14702,
      "ci": 14165
    },
    "GeographicalInformation": {},
    "GeodeticInformation": {},
    "EPSLocationInformation": {
      "TAI": {},
      "ECGI": {},
      "GeographicalInformation": {},
      "GeodeticInformation": {}
    }
  },
  "PSLocationInformation": {
    "LSA": {},
    "RAI": {},
    "GeographicalInformation": {},
    "GeodeticInformation": {}
  },
  "GPRSMSClass": {},
  "MNPInfoResult": {},
  "saiPresent": false,
  "ageOfLocationInformation": 0,
  "currentLocationRetrieved": true,
  "msisdn": 601140827446,
  "imsi": 502194100959766,
  "vlrNumber": 60193900083,
  "subscriberState": "camelBusy"
}
---

** OFF - before 4 hours **

{
  "network": "UMTS",
  "protocol": "MAP",
  "operation": "PSI",
  "subscriberIdentity": 601140827446,
  "result": "ERROR",
  "errorReason": "[MAP Dialog Timeout]"
}

{
  "network": "GSM",
  "protocol": "MAP",
  "operation": "PSI",
  "result": "SUCCESS",
  "CSLocationInformation": {
    "LocationNumber": {
      "oddFlag": true,
      "natureOfAddressIndicator": 4,
      "internalNetworkNumberIndicator": 1,
      "numberingPlanIndicator": 1,
      "addressPresentationRestrictedIndicator": 1,
      "screeningIndicator": 3,
      "address": 60199030053
    },
    "CGI": {
      "mcc": 502,
      "mnc": 19,
      "lac": 14702,
      "ci": 14165
    },
    "GeographicalInformation": {},
    "GeodeticInformation": {},
    "EPSLocationInformation": {
      "TAI": {},
      "ECGI": {},
      "GeographicalInformation": {},
      "GeodeticInformation": {}
    }
  },
  "PSLocationInformation": {
    "LSA": {},
    "RAI": {},
    "GeographicalInformation": {},
    "GeodeticInformation": {}
  },
  "GPRSMSClass": {},
  "MNPInfoResult": {},
  "saiPresent": false,
  "ageOfLocationInformation": 3,
  "currentLocationRetrieved": false,
  "msisdn": 601140827446,
  "imsi": 502194100959766,
  "vlrNumber": 60199030053,
  "subscriberState": "assumedIdle"
}
---

** IMSIDETACH - after 4 Hours **

{
  "network": "GSM",
  "protocol": "MAP",
  "operation": "PSI",
  "result": "SUCCESS",
  "CSLocationInformation": {
    "LocationNumber": {
      "oddFlag": true,
      "natureOfAddressIndicator": 4,
      "internalNetworkNumberIndicator": 1,
      "numberingPlanIndicator": 1,
      "addressRepresentationRestrictedIndicator": 1,
      "screeningIndicator": 3,
      "address": 60199030053
    },
    "CGIorSAIorLAI": {
      "mcc": 502,
      "mnc": 19,
      "lac": 14702,
      "ci": 14165
    },
    "GeographicalInformation": {},
    "GeodeticInformation": {},
    "EPSLocationInformation": {
      "TAI": {},
      "ECGI": {},
      "GeographicalInformation": {},
      "GeodeticInformation": {}
    }
  },
  "PSLocationInformation": {
    "LSA": {},
    "RAI": {},
    "CGIorSAIorLAI": {},
    "GeographicalInformation": {},
    "GeodeticInformation": {}
  },
  "GPRSMSClass": {},
  "MNPInfoResult": {},
  "saiPresent": false,
  "ageOfLocationInformation": 248,
  "currentLocationRetrieved": false,
  "msisdn": 601140827446,
  "imsi": 502194100959766,
  "vlrNumber": 60199030053,
  "subscriberState": "netDetNotReachable",
  "notReachableReason": "imsiDetached"
}
---
** OFF **

{
  "network": "UMTS",
  "protocol": "MAP",
  "operation": "SRIforSM",
  "subscriberIdentity": 601140827446,
  "result": "ERROR",
  "errorReason": "[MAP Component error: Absent Subscriber SM, MAP error code value: 6]"
}
---

** ECGI & TAI - (LTE) **

{
  "network": "GSM",
  "protocol": "MAP",
  "operation": "PSI",
  "result": "SUCCESS",
  "CSLocationInformation": {
    "LocationNumber": {
      "oddFlag": true,
      "natureOfAddressIndicator": 4,
      "internalNetworkNumberIndicator": 0,
      "numberingPlanIndicator": 1,
      "addressPresentationRestrictedIndicator": 3,
      "screeningIndicator": 3,
      "address": 60181000011
    },
    "GeographicalInformation": {},
    "GeodeticInformation": {},
    "EPSLocationInformation": {
      "TAI": {
        "mcc": 502,
        "mnc": 18,
        "tac": 1028
      },
      "ECGI": {
        "mcc": 502,
        "mnc": 18,
        "eNBId": 811144,
        "ci": 101
      },
      "GeographicalInformation": {},
      "GeodeticInformation": {}
    }
  },
  "PSLocationInformation": {
    "LSA": {},
    "RAI": {},
    "GeographicalInformation": {},
    "GeodeticInformation": {}
  },
  "GPRSMSClass": {},
  "MNPInfoResult": {},
  "saiPresent": false,
  "currentLocationRetrieved": true,
  "msisdn": 60182115804,
  "imsi": 502181133119394,
  "vlrNumber": 60181000011,
  "mscNumber": 60181000011,
  "subscriberState": "assumedIdle"
}

** SRISM-PSI with typeofshape and imei **		


{
  "network": "GSM",
  "protocol": "MAP",
  "operation": "PSI",
  "result": "SUCCESS",
  "CSLocationInformation": {
    "LocationNumber": {
      "oddFlag": false,
      "natureOfAddressIndicator": 4,
      "internalNetworkNumberIndicator": 1,
      "numberingPlanIndicator": 1,
      "addressPresentationRestrictedIndicator": 1,
      "screeningIndicator": 3,
      "address": 2012100165336279
    },
    "SAI": {
      "mcc": 602,
      "mnc": 1,
      "lac": 1653,
      "sac": 36279
    },
    "GeographicalInformation": {
      "typeOfShape": "EllipsoidPointWithUncertaintyCircle"
    },
    "GeodeticInformation": {},
    "EPSLocationInformation": {
      "TAI": {},
      "ECGI": {},
      "GeographicalInformation": {},
      "GeodeticInformation": {}
    }
  },
  "PSLocationInformation": {
    "LSA": {},
    "RAI": {},
    "GeographicalInformation": {},
    "GeodeticInformation": {}
  },
  "GPRSMSClass": {},
  "MNPInfoResult": {},
  "saiPresent": true,
  "ageOfLocationInformation": 0,
  "currentLocationRetrieved": true,
  "msisdn": 60193303030,
  "imsi": 502193907608104,
  "imei": "359447099238100",
  "vlrNumber": 201222000816,
  "mscNumber": 201222000816,
  "subscriberState": "assumedIdle"
}
